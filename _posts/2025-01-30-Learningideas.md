---
layout: post
title: 'Learning & Books'
description: 'Best books on Java platform'
date:   2020-01-30 06:15:41 -0300
categories: start blog
by: 'Sam Soltani'
icon: 'book'
questions:
  - question: 'Microsubjects'
    answer: 'Today I try to learn a subject and learn that very well. For example, today I am working on Observer Design Pattern and will not let go until what it returns is something which satisfies me.'
    image: "3.gif"
  - question: 'EJB'
    answer: 'Enterprise Java Beans.With enterprise javabeans we can develop building blocks - EJB components - We or someone else can assemble and re-assemble into various applications.'
    image: "3.gif"
  - question: 'Servlet'
    answer: 'Servlets are applets that are designed to run on a server rather than on a web browser. Servlet are used to extent the capabilities of the server'
    image: "3.gif"
  - question: 'JSON-P'
    answer: 'Stands for JSON Processing. This is a low level Java API which is used to generate, parse and query JSON documents.
    JSON-P is based on two models:'
    image: "3.gif"
  - question: 'JSON-B (Binding) API'
    answer: 'Java EE 8 feature, JSR-367, JSON-B API provides a uniform standard for converting Java to JSON and back again. There are also 3rd party converters like google Gson or Jackson.
    JSON-B API is easy to configure, simple to use.'
    image: "1.gif"

---


Above are glossary of terms used in Java platform.
