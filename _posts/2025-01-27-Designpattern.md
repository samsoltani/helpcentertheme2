---
layout: post
title: 'Design Patterns'
description: 'A Summary of most used design patterns'
date:   2020-01-26 17:46:41 -0300
categories: start blog
by: 'Sam Soltani'
icon: 'grid'
questions:
  - question: 'Strategy Pattern'
    answer: 'GOF definition - Define of family of algorithms, encapsulate each one and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.'
    image: "3.gif"
  - question: 'Observer Pattern'
    answer: 'Objects or observers get notified when the subject changes state.
    
    Some real examples: A newspaper company and its subscribers are a good example. It is a one-to-many relationship because the newspaper company can have many subscribers (observers) which get notified when there is an update.
    
    Another good example can be a celebrity and her numorous fans. A fan or follower (observer) would like to receive updates i.e. get notified when something about the celebrity (subject) changes.'
    image: "3.gif"
  - question: 'Singleton'
    answer: 'GOF definition - Ensure that a class has only one instance and provide a global point of access to it.'
    image: "3.gif"
  - question: 'Momento'
    answer: 'GOF definition - Without violating encapsulation, capture and externalize an object internal state so that the object can be restored to this state later'
    image: "1.gif"

---

Who is into OOP and hasn't heard of Gang of Four? This classic book is a must read for everyone who want to understand what design patterns are.
