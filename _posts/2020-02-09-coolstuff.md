---
layout: post
title: 'Cool Stuff'
description: 'Some of the cool things in technology world'
date:   2020-02-05 03:46:41 -0300
categories: start blog
by: 'Sam Soltani'
icon: 'headphones'
questions:
  - question: 'Rouge'
    answer: 'Loosely coupled objects are objects that are connected and coupled but know very little about eachother. We can see "Loose Coupling" in patterns like "Observer".'
    image: "3.gif"
  - question: 'Prism'
    answer: 'Enterprise Java Beans.With enterprise javabeans we can develop building blocks - EJB components - We or someone else can assemble and re-assemble into various applications.'
    image: "3.gif"
  - question: 'Servlet'
    answer: 'Servlets are applets that are designed to run on a server rather than on a web browser. Servlet are used to extent the capabilities of the server'
    image: "3.gif"
  - question: 'JSON-P'
    answer: 'Stands for JSON Processing. This is a low level Java API which is used to generate, parse and query JSON documents.
    JSON-P is based on two models:'
    image: "3.gif"
  - question: 'JSON-B (Binding) API'
    answer: 'Java EE 8 feature, JSR-367, JSON-B API provides a uniform standard for converting Java to JSON and back again. There are also 3rd party converters like google Gson or Jackson.
    JSON-B API is easy to configure, simple to use.'
    image: "1.gif"

---


Above are glossary of terms used in Java platform.
