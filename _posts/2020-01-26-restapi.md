---
layout: post
title: 'Rest APIs'
description: 'Getting Oracle Certifications'
date:   2017-11-12 17:46:41 -0300
categories: start blog
by: 'Sam Soltani'
icon: 'share-2'
questions:
  - question: 'What is REST?'
    answer: 'REST is an architectural style. REST stands for Representational State Transfer and it was created as a PhD thesis by Roy Fielding. It means that whenever a request is sent, the server responds by representing the state of that resource.'
    image: "3.gif"
  - question: 'Clients and Servers'
    answer: 'Before we begin with REST API, let me explain two things which are very important when it comes to REST API. They are **Client** and **Resource**.'
    image: "3.gif"
  - question: 'Constraints'
    answer: 'When we want to write RESTful APIs, we have to follow a set of constraints. These constraints will make our APIs easier to find and work with for other developers.
    These constraints are:
    -Stateless
    -Code on demand
    -Client Server seperation
    -Casheable
    -Layered System
    -Uniform Interface'
    image: "3.gif"
  - question: 'Interface Segregation Principle'
    answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    image: "3.gif"
  - question: 'Dependency Inversion Principle'
    answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    image: "3.gif"
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
